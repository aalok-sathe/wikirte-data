#!/usr/bin/env python3

import mwparserfromhell as mwp
from crawlprime_db1 import load_data
import json
from progressbar import progressbar

db = './out2/db_2.json'

titles_dict = dict()
reverse_lookup = dict()

def get_wikilink_titles(wikicode):
    w = mwp.nodes.Wikilink
    titles = []
    for node in wikicode.nodes:
        if type(node) is w:
            t = str(node.title)
            if ':' in t: continue
            if t not in titles_dict:
                titles_dict[t] = len(titles_dict)
                reverse_lookup[titles_dict[t]] = t
            titles.append(titles_dict[t])
    return titles

with open(db, 'r') as inp:
    data = json.load(inp)

new_entries = []
for i, entry in enumerate(progressbar(data['sentences'])):
    pid = entry['wikipageid']
    with open('xml/%d.xml'%pid, 'r') as inp:
        wikicode, pid, rid = load_data(inp.read())
    titles = get_wikilink_titles(wikicode)

    entry['id'] = i
    entry['wikilinks'] = titles

    new_entries.append(entry)

with open('./out2/db_2_prime.json', 'w') as out:
    data = dict(sentences=new_entries, wikilinks=reverse_lookup)
    json.dump(data, out, indent=4)
