import json
import os
import sys
import logging
from argparse import ArgumentParser
# from concurrent.futures import ProcessPoolExecutor
from functools import partial
from hashlib import md5
from mw import xml_dump

import requests
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage

from stream_resp import ResponseStream

"""Download references. Expects data to be in `out` directory."""

logger = logging.getLogger('ref_dl')

arg_parser = ArgumentParser()
arg_parser.add_argument('-p', '--parallelism', type=int,
                        help='number of worker processes to run', default=32)
arg_parser.add_argument('-o', '--overwrite', type=bool,
                        help='overwrite old output', default=False)
arg_parser.add_argument('-nd', '--no-download', type=bool,
                        help='do not download files again', default=False)
config = arg_parser.parse_args(args='')

req_headers = {'user-agent':
               'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:64.0) '
               'Gecko/20100101 Firefox/64.0'}

bad_set = set()
if os.path.exists('bad.txt'):
    with open('bad.txt') as bad_f:
        bad_set = {line.strip() for line in bad_f.readlines()}


def check_resp(requests_callable, ident):
    # noinspection PyBroadException
    try:
        resp = requests_callable()
    except requests.Timeout:
        # print('{} timed out'.format(ident), file=sys.stderr)
        logger.debug('{} timed out'.format(ident))
        return None
    except requests.ConnectionError:
        # print('{} failed to connect'.format(ident), file=sys.stderr)
        logger.debug('{} failed to connect'.format(ident))
        return None
    except Exception as exc:
        # print('unexpected exception: {}'.format(exc), file=sys.stderr)
        logger.exception('unexpected exception in request')
        return None

    if not (200 <= resp.status_code < 299):
        print('{} status {}'.format(ident, resp.status_code), file=sys.stderr)
        # logger.debug('{} status {}'.format(ident, resp.status_code))
        return None

    cnt_type = resp.headers.get('content-type', '').lower()
    if cnt_type != 'x-pdf' and cnt_type != 'application/pdf':
        print('INFO: {} ignored (not pdf)'.format(ident), file=sys.stderr)
        # logger.debug('{} ignored (not pdf)'.format(ident))
        return None

    cnt_length = int(resp.headers.get('content-length', '0'))
    if cnt_length > 100000000:  # don't download anything bigger than 50mb
        print('INFO: {} ignored (too large)'.format(ident), file=sys.stderr)
        # logger.debug('{} ignored (too large)'.format(ident))
        return None

    return resp


def download_references(filename):
    local_bad_set = set()

    how_many_skipped = 0

    print('downloading references for {}'.format(filename), file=sys.stderr)
    # logger.info('downloading references for {}'.format(filename))

    _rsrcmgr = PDFResourceManager()

    with open(filename) as f:
        data = json.load(f)

    for sent in data['sentences']:
        for ref in sent['links']:
            # print('handling link {}'.format(ref), file=sys.stderr)
            logger.debug('handling link {}'.format(ref))

            m = md5()
            m.update(ref.encode('utf-8'))
            digest = m.hexdigest()

            if digest in local_bad_set or digest in bad_set:
                print('ref {} ignored (known bad)'.format(ref),
                      file=sys.stderr)
                how_many_skipped += 1
                # logger.debug('ref {} ignored (known bad)'.format(ref))
                continue

            tgt_file = 'refdata/{}.txt'.format(digest)

            if os.path.exists(tgt_file):# and not config.overwrite:
                # print('ref {} ignored (file existed)'.format(ref),
                      # file=sys.stderr)
                logger.debug('ref {} ignored (file existed)'.format(ref))
                continue
            elif config.no_download:
                local_bad_set.add(digest)
                how_many_skipped += 1
                continue

            resp = check_resp(partial(requests.head, ref, headers=req_headers,
                                      allow_redirects=True, timeout=1),
                                      'ref {}'.format(ref))

            if not resp:
                how_many_skipped += 1
                local_bad_set.add(digest)
                continue

            print('downloading ref {}'.format(ref), file=sys.stderr)
            # logger.info('downloading ref {}'.format(ref))
            resp = check_resp(partial(requests.get, ref, headers=req_headers,
                                      timeout=3, stream=True),
                              'ref {}'.format(ref))
            if not resp:
                how_many_skipped += 1
                local_bad_set.add(digest)
                continue

            print('ref {} successfully downloaded'.format(ref), file=sys.stderr)
            # logger.debug('ref {} successfully downloaded'.format(ref))

            print('parsing ref {}'.format(ref), file=sys.stderr)
            # logger.debug('parsing ref {}'.format(ref))

            body = resp.raw
            body.decode_content = True
            body = ResponseStream(body)

            with open(tgt_file, 'w', encoding='utf-8') as f:
                _device = TextConverter(_rsrcmgr, f, codec='utf-8', laparams=LAParams())
                pdfinterp = PDFPageInterpreter(_rsrcmgr, _device)

                for page in PDFPage.get_pages(body):
                    pdfinterp.process_page(page)

                del _device
                del pdfinterp

            print('wrote text for {}'.format(ref), file=sys.stderr)
            # logger.info('wrote text for {}'.format(ref))

    with open('bad.txt', 'a') as f:
        f.writelines('{}\n'.format(elem) for elem in local_bad_set.difference(bad_set))

    bad_set.update(local_bad_set)

    return how_many_skipped

def main():
    if not os.path.exists('refdata'):
        os.mkdir('refdata')

    if not os.path.exists('logs'):
        os.mkdir('logs')

    logging.getLogger('pdfminer').setLevel(logging.WARNING)
    logging.getLogger('requests').setLevel(logging.INFO)
    logging.getLogger('urllib3').setLevel(logging.INFO)

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(logging.Formatter('%(name)s - %(levelname)s - %(message)s'))
    # stream_handler.setLevel(logging.INFO)

    file_handler = logging.FileHandler('logs/dl.log', mode='w')
    file_handler.setFormatter(logging.Formatter('[%(asctime)s | %(name)s | %(levelname)s] %(message)s',
                                                '%Y-%m-%d %H:%M:%S'))
    file_handler.setLevel(logging.INFO)

    root_logger.addHandler(stream_handler)
    root_logger.addHandler(file_handler)

    logger.info('listing output files...')
    files = os.listdir('out')

    logger.info('starting {} threads'.format(config.parallelism))
    with ProcessPoolExecutor(config.parallelism) as tx:
    for file in files:
        if file.split('.')[-1] != 'json': continue
        # if file != '39.json': continue # DEBUG
        # tx.submit(download_references, 'out/{}'.format(file))
        how_many_skipped = download_references('out/{}'.format(file))
        print('INFO: {} links skipped'.format(how_many_skipped))



if __name__ == '__main__':
    main()
