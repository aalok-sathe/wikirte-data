#!/usr/bin/env python3
# crawlprime.py
# aalok s. (GitLab:@aalok-sathe) (aalok.sathe@richmond.edu)
# Joonsuk Park (park@joonsuk.org)
# (C) 2018-2019
# code adapted from `crawl.py' by GH:@mammothbane, GH:@michaelwong2
"""
This file serves to help extract data from wikipedia dumps. When run as main,
it will look for a wiki.xml.bz2 (or similarly named) wiki data dump and extract
pages from it one after another.
When imported as a library, the `handle_page` function is useful to pass an
XML wiki article to and initiate parsing.
Please see individual function docstrings for more information and usage.
Particularly, to process only a single page at a time, import crawlprime.py
and call handle_page(page) on a `page' that is the raw text of a wiki XML file.
"""

import json
import pickle
import logging
import os.path
import unicodedata
import re
import sys
import random
from datetime import datetime
from hashlib import md5
from argparse import ArgumentParser
from color import INFO, ERR
from collections import Counter
from progressbar import progressbar
from glob import glob
from concurrent.futures import ProcessPoolExecutor
from functools import partial
from typing import List
import lxml.etree as et
import mwparserfromhell as mwp
import nltk
from nltk.tokenize import PunktSentenceTokenizer, BlanklineTokenizer
from nltk.tree import Tree
import spacy
from pycorenlp import StanfordCoreNLP

nlp = StanfordCoreNLP('http://localhost:9000')
# spacy_nlp = spacy.load('en_core_web_sm')

logger = logging.getLogger('postprocess')
# WIKI_FILE = 'wiki.xml'
# OPEN_FN = partial(open, WIKI_FILE, 'r', encoding='utf-8')
NOW = datetime.now().strftime('%Y%m%d_%H%M%S')

global excl, totals, new_sents
excl = Counter()
totals = 0
new_sents = []

def post_process(text):
    """
    post_process
    takes in a string and applies post-processing filters to make the text
    well-formatted.
    Note: the order of post-processing operations is important.
    ---
    text: str := raw text (not wikicode or anything else) that is usually the
                 output of wiki_strip or
    """
    text = re.sub(r'WHYEXCL:[a-z]+(?= +)', '', text)
    text = re.sub(r'EXCLUDE_THIS_ENTRY', '', text)
    text = re.sub(r'"', ' ', text)
    text = re.sub(r'\n+', '', text)
    text = re.sub(r' +', ' ', text)
    text = re.sub(r' +,', ',', text)
    text = re.sub(r' +\.', '.', text)
    text = re.sub(r'^ +', '', text)
    text = re.sub(r',$', '', text)
    text = re.sub(r' $', '', text)

    return text

def is_well_formed(text=None):
    doc = spacy_nlp(text)

    # check if some token in the chunk of text is a verb. return False if not.
    for token in doc:
        # print(token.pos_)
        if token.pos_ == 'VERB': break
    else: # note the purposeful de-indent. see 'for-else' syntax
        print("INFO: not well formed:", text)
        return False

    # there can potentially be more well-formedness checks here. leaving space.

    return True


def load_filter_lists():
    with open('list_ref_4.txt', 'r') as file:
        valids = {line[:-1] for line in file}
    #with open('filter_pick_ref.txt', 'r') as file:
    #    valids.intersection_update({line[:-1] for line in file})
    alll = {filename[:-4] for filename in os.listdir('refdata')}
    return valids, alll


def reffilename(link):
    hashobj = md5()
    hashobj.update(link.encode('utf-8'))
    digest = hashobj.hexdigest()
    return digest


def is_ref_good(text, index, hash, reffilesets):
    """
    """

    valids, alll = reffilesets

    if hash not in valids:
        # INFO('HASH')
        # print('{} was not found in valid ref files'.format(hash))
        # return False
        if hash in alll:
            pass
            # INFO('HASH')
            # print('{} was in all refs but not valid refs'.format(hash))
        else:
            pass
            # INFO('HASH')
            # print('{} was not found anywhere'.format(hash))
        return False
    else:
        INFO('HASH!!')
        # print('found {} in valids'.format(hash))


    span = text[:index]
    output = nlp.annotate(span, properties={'annotators': 'parse',
                                            'outputFormat': 'json'})
    parse = output['sentences'][0]['parse']
    tree = Tree.fromstring(parse)
    # INFO()
    # print(text, tree)
    if tree.label() == 'ROOT':
        try:
            if tree[0].label() != 'S':
                return False
        except IndexError:
            ERR()
            print('reached indexerror on', text)
            return False
    elif tree.label() != 'S':
        return False

    if r'\u' in span.encode('unicode-escape').decode('ascii'):
        excl['unicode'] += 1
        return False

    if span.endswith(':'):
        excl['bad_list'] += 1
        return False

    return True


def get_good_claim(claim, i):
    """
    Before actually adding the claim to db, make sure that claim[:i] actually
    forms a valid claim. We run
    """
    pass


def process_page(filename, reffilesets, config, reverse=True):
    """
    """
    INFO()
    print('extracting from page {}'.format(filename), end='\r')

    new_sents = []

    with open(filename, 'r') as file:
        data = json.load(file)

    sentences = data['sentences']
    pageid = data['id']

    for i, sent in enumerate(sentences):
        text = sent.pop('text')
        ctxt = sent['context']
        refs = sent.pop('refs')

        iterable = sorted(refs, key=lambda entry: entry['index'])
        if reverse:
            iterable = reversed(iterable)
        for entry in iterable:
            index, links = entry['index'], entry['links']
            goodlink = None
            # INFO('DEBUG')
            # print('processing', (text, index))
            for link in links:
                hash = reffilename(link)
                # ERR('INFO')
                # print(link, hash)
                if is_ref_good(text, index, hash, reffilesets):
                    goodlink = (link, hash)
                    break
                else:
                    continue
            if goodlink:
                global totals
                totals += 1
                if config.breaksents:
                    sent['claim'] = text[:index]
                else:
                    sent['claim'] = text# text[:index]
                sent['context'] = ctxt
                sent['evidence'] = goodlink[1] + '.txt'
                sent['url'] = goodlink[0]
                sent['wikipageid'] = pageid
                new_sents.append(sent)
                # INFO('VALID FOUND')
                # print('found: ', sent)
                print('included:', totals, 'excluded:', excl)
                break
            else:
                continue
        else:
            # INFO()
            # print('NO REFS FOUND in page {}'.format(pageid))
            excl['bad_refs'] += 1
            continue

    return new_sents

def main(config):
    if config.parallelism != 1:
        INFO('WARNING')
        print("program started with parallelism {}".format(config.parallelism))

    if not os.path.exists('out2'):
        os.mkdir('out2')

    if not os.path.exists('logs'):
        os.mkdir('logs')

    # logging stuff
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(logging.Formatter('%(name)s - %(levelname)s '
                                                  '- %(message)s'))

    file_handler = logging.FileHandler('logs/crawl2.log', mode='w')
    file_handler.setFormatter(logging.Formatter('[%(asctime)s | %(name)s | '
                                                '%(levelname)s] %(message)s',
                                                '%Y-%m-%d %H:%M:%S'))
    root_logger.addHandler(stream_handler)
    root_logger.addHandler(file_handler)

    logger.info('starting {} threads'.format(config.parallelism))

    INFO()
    print('using db_1 from {} to create db_2 at {}'.format('out','out2'))

    valids, alll = load_filter_lists()
    # INFO()
    # print(valids)

    # with ProcessPoolExecutor(config.parallelism) as tx:
    files = glob('./out/*.json')
    new_sents = []

    numprocessed = 0
    for file in progressbar(files, redirect_stdout=True):
        if config.parallelism > 1:
            raise NotImplementedError
            # tx.submit(process_page, file, valids)
        else:
            try:
                new_sents += process_page(file, (valids, alll), config, reverse=config.reverse)
            except TypeError:
                continue

        # INFO()
        # print("processed {} pages".format(i))

        if config.test and numprocessed >= config.test:
            INFO()
            print('reached end of test run; stopping')
            break

        ERR('PROGRESS --> ')
        print('{} percent processed'.format(100*numprocessed/len(files)), end='\r')

        numprocessed += 1

    if config.breaksents:
        outname = 'db_2'
    else:
        outname = 'db_2_fullsents'

    # shuffle claims list
    random.shuffle(new_sents)

    with open('out2/{}.json'.format(outname), 'w') as out:
        INFO()
        print('writing data to {}'.format('out2/db_2.json'))
        # print(new_sents[:10])
        json.dump({'sentences': new_sents}, out, indent=4)

    with open('logs/stats_{}_{}.txt'.format(outname, NOW), 'a') as statsfile:
        INFO()
        print('excluded', str(excl))
        print('included: {}'.format(totals))
        print("processed {} pages".format(numprocessed))
        print('excluded', str(excl), file=statsfile)
        print('included: {}'.format(totals), file=statsfile)
        print("processed {} pages".format(numprocessed), file=statsfile)

if __name__ == '__main__':
    arg_parser = ArgumentParser()
    arg_parser.add_argument('-r', '--reverse', type=int,
                            help='pick last citation first or first one first', default=1)
    arg_parser.add_argument('-p', '--parallelism', type=int,
                            help='number of worker processes to run', default=1)
    arg_parser.add_argument('-o', '--overwrite', type=bool,
                            help='overwrite old output', default=True)
    arg_parser.add_argument('-t', '--test', type=int,
                            help='run only for a small number', default=0)
    arg_parser.add_argument('-b', '--breaksents', type=int,
                            help='break sentences at citation loc',
                            default=1)
    config = arg_parser.parse_args()

    print(config)

    # raise SystemExit
    main(config)
