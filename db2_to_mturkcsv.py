#!/usr/bin/env python3
import json
import csv
from progressbar import progressbar
import random

file = 'out2/db_2_prime.json'
with open(file, 'r') as datafile:
    db = json.load(datafile)
    data = db['sentences']
    lookup = db['wikilinks']

lenlim = input('upper limit on the length of claims? [35]\t')
if not lenlim:
    lenlim = 15
lenlim = int(lenlim)

data = [entry for entry in data if len(entry['claim'].split(' ')) <= lenlim]

howmany = input('how many claims per page?\t')
howmany = int(howmany)
spl = input('how many entries to include? [e.g. 1000]\nEnter `0` for all\t')
if spl == '' or int(spl) != 0:
    data = data[:int(spl)]

howmanywikilinks = input('how many random wikilink titles to pick? [20]\t')
if not howmanywikilinks:
    howmanywikilinks = 20
howmanywikilinks = int(howmanywikilinks)

def well_formed(wikititle):
    excl = {'(', ')', '_', '#', *[str(i) for i in range(10)]}
    chrs = set(wikititle)
    intsct = excl.intersection(chrs)
    return len(intsct) == 0

with open('mturk_csv_%d_%d_%d.csv'%(howmany, int(spl), lenlim), 'w') as out:
    writer = csv.writer(out, delimiter=',', dialect='excel')
    row = []
    for j in range(howmany):
        row += ['id%d'%(howmany-j), 'claim%d'%(howmany-j), 'length%d'%(howmany-j), 'wikilinks%d'%(howmany-j)]
    writer.writerow([*row])
    for i in progressbar(range(howmany-1, len(data), howmany)):
        row = []
        for j in range(howmany):
            this_titles = [lookup[str(x)] for x in data[i-j]['wikilinks']]
            this_titles = [*filter(well_formed, this_titles)]
            random.shuffle(this_titles)
            row += [0*(i-j+1) + data[i-j]['id'], data[i-j]['claim'], len(data[i-j]['claim'].split()),
                    ', '.join(this_titles[:howmanywikilinks])]
        writer.writerow([*row])
