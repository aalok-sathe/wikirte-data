#!/usr/bin/env python3
# crawlprime.py
# aalok s. (GitLab:@aalok-sathe) (aalok.sathe@richmond.edu)
# Joonsuk Park (park@joonsuk.org)
# (C) 2018-2019
# code adapted from `crawl.py' by GH:@mammothbane, GH:@michaelwong2
"""
This file serves to help extract data from wikipedia dumps. When run as main,
it will look for a wiki.xml.bz2 (or similarly named) wiki data dump and extract
pages from it one after another.
When imported as a library, the `handle_page` function is useful to pass an
XML wiki article to and initiate parsing.
Please see individual function docstrings for more information and usage.
Particularly, to process only a single page at a time, import crawlprime.py
and call handle_page(page) on a `page' that is the raw text of a wiki XML file.
"""

import json
import logging
import os.path
import unicodedata
import re
import sys
import color
from argparse import ArgumentParser
from concurrent.futures import ProcessPoolExecutor
from functools import partial
from typing import List
import lxml.etree as et
import mwparserfromhell as mwp
import nltk
# import spacy
from nltk.tokenize import PunktSentenceTokenizer, BlanklineTokenizer
from collections import Counter
# import glob
from datetime import datetime

# spacy_nlp = spacy.load('en_core_web_sm')
if not os.path.isfile(os.path.expanduser('~') +
                      '/nltk_data/tokenizers/punkt/english.pickle'):
    nltk.download('punkt')

# logger = logging.getLogger('crawl')
WIKI_FILE = 'wiki.xml'
OPEN_FN = partial(open, WIKI_FILE, 'r', encoding='utf-8')
NOW = datetime.now().strftime('%Y%m%d_%H%M%S')

global excl, totals
excl = Counter()
totals = 0

def post_process(text):
    """
    post_process
    takes in a string and applies post-processing filters to make the text
    well-formatted.
    Note: the order of post-processing operations is important.
    ---
    text: str := raw text (not wikicode or anything else) that is usually the
                 output of wiki_strip or
    """
    text = re.sub(r'WHYEXCL:[a-z]+(?= +)', '', text)
    text = re.sub(r'EXCLUDE_THIS_ENTRY', '', text)
    text = re.sub(r'\\u[0-9]+', '', text) # unicode chars
    text = re.sub(r'"', ' ', text) # quotation marks
    text = re.sub(r'\n+', '', text) # excess newlines
    text = re.sub(r' +', ' ', text) # too much whitespace
    text = re.sub(r' +,', ',', text) # badly placed commas
    text = re.sub(r' +\.', '.', text) # badly placed periods
    text = re.sub(r'^ +', '', text) # initial whitespace
    text = re.sub(r',$', '', text) # sentence-final commas
    text = re.sub(r' $', '', text) # sentence-final whitespace

    return text

# dummy method that returns only True for now
def is_well_formed(text=None):

    # doc = spacy_nlp(text)
    #
    # # check if some token in the chunk of text is a verb. return False if not.
    # for token in doc:
    #     # print(token.pos_)
    #     if token.pos_ == 'VERB': break
    # else: # note the purposeful de-indent. see 'for-else' syntax
    #     print("INFO: not well formed:", text)
    #     return False

    # there can potentially be more well-formedness checks here. leaving space.

    return True

def wiki_strip(in_nodes, normalize=True, collapse=False,#True,
               keep_template_params=False):
    """
    wiki_strip
    takes in a list of mwp.node.Node objects and returns a string that is
    concatenated text from all the nodes, stripped of all wikicode annotations
    ---
    in_nodes: list := list of mwp.node.Node objects
    """
    kwargs = {
        "normalize": normalize,
        "collapse": collapse,
        "keep_template_params": keep_template_params
    }

    nodes = []
    for node in in_nodes:
        stripped = node.__strip__(**kwargs)
        if stripped and 'thumb|' not in stripped:
            nodes.append(str(stripped))

    if collapse:
        stripped = "".join(nodes).strip("\n")
        while "\n\n\n" in stripped:
            stripped = stripped.replace("\n\n\n", "\n\n")
        returnable = stripped
    else:
        returnable = "".join(nodes)

    return returnable

def load_data(page):
    """
    load_data:
    takes in a wiki page as an XML document (as stored in wikipedia dumps)
    and returns the raw wikicode, pageid, and revid.
    ---
    page: str := raw text from an xml document representing a wiki page
    """
    page = et.fromstring(page)
    page_id = int(page.xpath('id')[0].text)
    rev_id = int(page.xpath('revision/id')[0].text)
    wikicode = page.xpath('revision/text')[0].text

    return mwp.parse(wikicode), page_id, rev_id


def parse_text(ast): # -> (List[(int,List[str])], str):
    """
    parse_text
    takes in raw wikicode and returns refs, text_acc, where refs is a list of
    (i, links) pairs, and text_acc is a str object, such that i is the index
    in text_acc where each link in links was cited as in-text reference.
    ---
    ast: str ::= raw wikicode of (usually) a wiki article
    """
    text_acc = ''
    refs = []
    global excl # Counter instance to count why stuff was excluded

    # print("INFO: in parse_text")

    # include_this = True
    for i in range(len(ast.nodes)):
        node = ast.nodes[i]
        this_text = unicodedata.normalize('NFKD', wiki_strip([node]))
        # print("INFO: node is a", type(node))

        if type(node) is not mwp.nodes.Heading and (type(node) is not
                                                    mwp.nodes.Tag or
                                                    str(node.tag) not in
                                                    {'ref','table'}):
            # this_text = unicodedata.normalize('NFKD', wiki_strip([node]))
            text_acc += ' ' + this_text

            if type(node) is mwp.nodes.Tag:
                tag = str(node.tag)
                if tag in {'math', 'sup', 'sub', 'code'}:
                    text_acc += ' EXCLUDE_THIS_ENTRY ' # add a silent piece of
                                                       # text to the stripped
                                                       # text for later exact
                                                       # string match
                                                       # since we don't yet
                                                       # know where the
                                                       # sentence breaks are,
                                                       # and we want to exclude
                                                       # that entire entry, not
                                                       # just the part
                                                       # containing the math
                    text_acc += ' WHYEXCL:%s ' % tag

            if type(node) is mwp.nodes.Template:
                contents = node.name.strip_code()
                # make sure it's not a math template
                if contents.lower() in {'math'}:
                    text_acc += ' EXCLUDE_THIS_ENTRY '
                    text_acc += ' WHYEXCL:%s ' % "math"
                if contents.lower() in {'convert'}:
                    text_acc += ' EXCLUDE_THIS_ENTRY '
                    text_acc += ' WHYEXCL:%s ' % "converttemplate"

            # exclude anything that has quoted stuff with more than 3 words
            if type(node) is mwp.nodes.Text:
                if '"' in str(node.value):
                    inquotes = re.findall(r'".+"', str(node.value))
                    # print("INFO: strings in quotes:", inquotes)
                    for quoted in inquotes:
                        if len(quoted.split()) > 3:
                            # print("INFO: excluded", inquotes)
                            text_acc += ' EXCLUDE_THIS_ENTRY '
                            text_acc += ' WHYEXCL:%s ' % "quote"
                            break

            # try:
            # print(this_text)
            if 'wikitable' in this_text:# or 'wikitable' in node.get('class'):
                # print(node)
                text_acc += ' EXCLUDE_THIS_ENTRY '
                text_acc += ' WHYEXCL:%s ' % "table"
                continue
            # except ValueError or AttributeError:
            #     pass

            continue

        if type(node) is mwp.nodes.Heading or not node.contents:
            # excl.update([type(node)]) # not excluding this, actually
            continue

        try:
            if 'wikitable' in node.get('class'):
                # print(node)
                text_acc += ' EXCLUDE_THIS_ENTRY '
                text_acc += ' WHYEXCL:%s ' % "table"
                continue
        except ValueError:
            pass

        # links = [*filter(lambda link: 'pdf' in str(link),
        #          node.contents.filter_external_links())]
        links = node.contents.filter_external_links()
        if len(links) == 0:
            excl.update(['no_links_found'])
            continue

        refs.append((len(text_acc), links))
        # include_this = True

    return refs, text_acc


def get_context(text, spans, nearestpar, i, start=None, lookback=5):
    # TODO
    """
    get_context
    a handler that extracts appropriate context for a claim-text.
    the method first tries to extract context using NLTK's BlanklineTokenizer,
    but if that returns a bad context, it picks up the 5 prior sentences,
    depending on how much context is available.
    ---
    text: str := the text of the wikipedia article being processed
    spans: list := output of NLTK Punkt's span_tokenize method
    nearestpar: int := index of the nearest previous paragraph boundary
    i: int := index of the particular Punkt sentences span being processed
    start: int := the index where the claim starts so we can exclude actual
                  claim from the context
    """
    if start is None:
        start = spans[i][0]
    if 1 <= start-nearestpar <= 5:
        ctx = text[nearestpar:start]
        return ctx
    try:
        ctx = text[spans[min([i-x for x in range(lookback) if i-x>=0])][0]:spans[i][0]]
        return ctx
    except IndexError:
        print("INFO: no context found!", file=sys.stderr)
        return ''

def get_ref_sentences(text, spans, pars, refs):
    """
    get_ref_sentences
    takes in the extracted (stripped) text of a wiki article, a list of index
    tuples generated by nltk PunktSentenceTokenizer, and a list, refs, that
    lists indices and links of references (produced by parse_text)
    ---
    text: str := stripped text from the relevant nodes of a wikicode article
                 (see parse_text and wiki_strip)
    spans: list := list of (start, end) index tuples produced by NLTK about
                   where it thinks a sentence begins and ends in `text`
    refs: List[(int, List[str])] := (see output description of parse_text)
    ---
    yields: (str, str, ist[str]), a tuple of text, context, and corresponding
                              referred evidence link that is yielded one after
                              another. calling list() on the generator will
                              produce a list of all claims and their citation
                              links found in `text`
    """

    global totals
    sent_ptr = 0
    prev_sent, prev_loc = None, None
    prev_good = None
    pending = list()

    refs_ptr = 0
    for i, (start, end) in enumerate(spans):
        this_links = []
        for index_ptr, (loc, links) in enumerate(refs[refs_ptr:]):
            refs_ptr = index_ptr
            if loc <= start:
                continue
            elif start < loc <= end:
                this_links += [(loc-start,
                                [link.url.strip_code() for link in links])]
            else:
                break
        if this_links and 'EXCLUDE_THIS_ENTRY' not in text[start:end]:
            nearestpar = min(pars, key=lambda x: loc-x + len(text)*int(loc<x))
            totals += 1
            if not text[start].istitle(): continue
            yield text[start:end], \
                  get_context(text, spans, nearestpar, i, start), \
                  this_links
        elif 'EXCLUDE_THIS_ENTRY' in text[start:end]:
            why = re.findall(r'(?<=WHYEXCL:)[a-z]+(?= +)', text[start:end])
            why = set(why) # avoid repeat-counting
            # print(text[start:end])
            # print("DEBUG:", why)
            excl.update(why)


def handle_page(page):
    """
    handle_page
    function to pre-process an XML page (by calling load_data) and passing
    on the data to various other functions. maintains 'output', i.e., a list
    of Dict[str: str, str: List] objects. finally, outputs the data to a
    unique file corresponding to the id of the page it processed.
    ---
    page: str := the raw XML string of a wiki page
    """
    ast, page_id, rev_id = load_data(page)
    filename = 'out/{}.json'.format(page_id)

    if os.path.exists(filename) and not config.overwrite:
        print('INFO: skipping {} (file already existed)'.format(filename))
        # logger.info('skipping {} (file already existed)'.format(filename))
        return None

    print('handling page with id {}'.format(page_id))
    # logger.info('handling page {}'.format(page_id))

    refs, text_acc = parse_text(ast)
    # UPDATE 2019: nltk tokenizers now return a generator? okay
    # unpack it into a list first
    sent_spans = [*PunktSentenceTokenizer().span_tokenize(text_acc)]
    par_bounds = [*sum(BlanklineTokenizer().span_tokenize(text_acc), ())]
    # print('INFO: sentence spans: %d'%len(sent_spans))
    ref_sents = get_ref_sentences(text_acc, sent_spans, par_bounds, refs)

    #output = [{'text': post_process(text),
    #           'context': post_process(ctx),
    #           'refs': [{'index': loc, 'links': links} for loc, links in refs]}
    #           for text, ctx, refs in ref_sents]

    output = []

    for text, ctx, refs in ref_sents:
        new_refs = []

        for loc, links in refs:
            orig_len = len(text[:loc])
            new_len = len(post_process(text[:loc]))
            delta =  orig_len - new_len
            # color.INFO(); print('delta=', delta)
            new_loc = loc - delta
            new_refs += [(new_loc, links)]
            #new_text += post_process(text[:loc])

        toadd = {'text': post_process(text),
                 'context': post_process(ctx),
                 'refs': [{'index': loc, 'links': links} for loc, links in
                                                         new_refs]}

        output.append(toadd)


    if len(output) == 0:
        # print('[NO_REF] {}'.format(page_id))
        # logger.debug('[NO_REF] {}'.format(page))
        return False

    with open(filename, 'w') as f:
        print("INFO: writing to", filename)
        json.dump({'id': page_id,
                   'revision': rev_id,
                   'sentences': output}, f, indent=4)

    with open('xml/{}.xml'.format(page_id), 'w') as f:
        print("INFO: writing xml for {}".format(id))
        print(page, file=f)

    global excl, totals
    print("INFO: excluded entries:", excl)
    print("INFO: no. of included entries:", totals)

    with open('logs/{}_stats.txt'.format(NOW), 'w') as statsfile:
        print("INFO: excluded entries:", excl, file=statsfile)
        print("INFO: no. of included entries:", totals, file=statsfile)

    return True

def main():
    if config.parallelism != 1:
        print('WARNING: you are trying to use parallelism', file=sys.stderr)
        # raise NotImplementedError("parallelism is unavailable for now")

    if not os.path.exists('out'):
        os.mkdir('out')

    if not os.path.exists('logs'):
        os.mkdir('logs')

    # logging stuff
    # root_logger = logging.getLogger()
    # root_logger.setLevel(logging.INFO)
    #
    # stream_handler = logging.StreamHandler()
    # stream_handler.setFormatter(logging.Formatter('%(name)s - %(levelname)s '
    #                                               '- %(message)s'))
    #
    # file_handler = logging.FileHandler('logs/crawl.log', mode='w')
    # file_handler.setFormatter(logging.Formatter('[%(asctime)s | %(name)s | '
    #                                             '%(levelname)s] %(message)s',
    #                                             '%Y-%m-%d %H:%M:%S'))
    # root_logger.addHandler(stream_handler)
    # root_logger.addHandler(file_handler)
    #
    # logger.info('starting {} threads'.format(config.parallelism))

    dumpfilename = ''

    numpages = 0
    print('INFO: beginning extraction', file=sys.stderr)

    # with ProcessPoolExecutor(config.parallelism) as tx, OPEN_FN() as f:
    with OPEN_FN() as f:

        # skip extraneous header
        try:
            with open('.OFFSET', 'r') as offsetfile:
                offset = offsetfile.read()
                offset = int(offset)
        except FileNotFoundError:
            print('file not found: .OFFSET')
            offset = 0
            line = True

            while line:
                line = f.readline()

                if '</siteinfo>' in line:
                    line = f.readline()
                    break

            with open('.OFFSET', 'w') as offsetfile:
                offsetfile.write(str(f.tell()))

        f.seek(offset)

        line = f.readline()

        acc = '' # accumulated text
        while line:
            if len(line) == 0:
                break

            acc += line
            if '</page>' in line:
                # tx.submit(handle_page, acc)
                handle_page(acc)
                acc = ''
                numpages += 1
                print("INFO: processed {:d} pages".format(numpages))

                with open('.OFFSET', 'w') as offsetfile:
                    offsetfile.write(str(f.tell()))

            if config.test and numpages >= config.test:
                break

            line = f.readline()

    with open('logs/{}_stats.txt'.format(NOW), 'a') as statsfile:
        color.INFO(); print("INFO: processed {} pages".format(numpages))
        print("INFO: processed {} pages".format(numpages), file=statsfile)

if __name__ == '__main__':
    arg_parser = ArgumentParser()
    arg_parser.add_argument('-p', '--parallelism', type=int,
                            help='number of worker processes to run', default=1)
    arg_parser.add_argument('-o', '--overwrite', type=int,
                            help='overwrite old output', default=1)
    arg_parser.add_argument('-t', '--test', type=int,
                            help='run only for a small number', default=0)
    config = arg_parser.parse_args()

    main()
