#!/usr/bin/env python3

import json
import argparse
from pathlib import Path
from tqdm import tqdm

def json_to_lines(db, keys_to_exclude=['wikilinks']):
    retdb = []
    for claim in tqdm(db):
        retdb.append({k:claim[k] for k in claim if k not in keys_to_exclude})
    return retdb


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('filepath', type=str, help='filename to convert to jsonl')
    parser.add_argument('-k', '--root_key', type=str, required=0,
                        help='what key to use at the root', default='sentences')
    parser.add_argument('-e', '--exclude_keys', type=str, required=0,
                        nargs='+', help='keys to exclude from each entry',
                        default='wikilinks')
    args = parser.parse_args()
    
    p = Path(args.filepath)
    db = json.load(p.open())
    if args.root_key is not None:
        db = db[args.root_key]

    newdb = json_to_lines(db, args.exclude_keys)

    p_prime = p.with_suffix('.jsonl')
    
    with p_prime.open('w+') as f:
        for entry in newdb:
            f.write(json.dumps(entry) + '\n')
