#!/usr/bin/env python3

import sys
import json
import random
from pathlib import Path
from nltk.tokenize import PunktSentenceTokenizer
from nltk import Tree
from functools import reduce
from progressbar import progressbar
import re

from pycorenlp import StanfordCoreNLP
nlp = StanfordCoreNLP('http://localhost:9000')


def binarize(tree):
    """
    Recursively turn a tree into a binary tree.
    """
    if isinstance(tree, str):
        return tree
    elif len(tree) == 1:
        return binarize(tree[0])
    else:
        label = tree.label()
        return reduce(lambda x, y: Tree(label, (binarize(x), binarize(y))), tree)


def get_parses(s):
    '''
    Returns a regular parse and a binary parse of s.
    '''

    if len(s) > 256:
        raise ValueError('string is too long')

    output = nlp.annotate(s, properties={
          'annotators': 'parse',
          'outputFormat': 'json'})
    # print(str(parse)[:1000])
    parse = output['sentences'][0]['parse']

    binary = binarize(parse)

    parse, binary = str(parse), str(binary)
    parse = re.sub('\n* +', ' ', parse)
    binary = re.sub('\n* +', ' ', binary)

    return parse, binary # (str, str)

def make_snli_entry(**kwargs):
    snli_entry_template = {
        "annotator_labels": ["neutral"],
        "captionID": "3416050480.jpg#4",
        "gold_label": "neutral",
        "pairID": "3416050480.jpg#4r1n",
        "sentence1": "A person on a horse jumps over a broken down airplane.",
        "sentence1_binary_parse": "",
        "sentence1_parse": "",
        "sentence2": "A person is training his horse for a competition.",
        "sentence2_binary_parse": "",
        "sentence2_parse": ""
    }

    for kw, val in kwargs.items():
        snli_entry_template[kw] = val

    return snli_entry_template


def entry_to_snliish_format(entry, label):
    '''
    converts wikiNLI db entry with label 'label' to SNLI format
    and uses appropriate corresponding fields.
    label must be one of '1' (positive) and '0' (negative)
    '''

    snli_entries = []

    ev_file = entry['evidence']
    try:
        with open('./refdata/wiki-refdata/%s'%ev_file, 'r') as f:
            evid = f.read().strip()
            evid = ''.join(map(lambda char: ' ' if char in '\n\t\r' else char, evid.strip()))
    except FileNotFoundError:
        raise

    spans = [*PunktSentenceTokenizer().span_tokenize(evid)]
    random.shuffle(spans)
    spans = spans[:32] # DEBUG

    numproc = 0
    print('processing all sentences for entry %d'%entry['claim_id'])
    for i, span in enumerate(progressbar(spans, redirect_stdout=0)):
        # print('span:\t', span)
        sent = evid[slice(*span)]
        snli_entry = make_snli_entry(
                annotator_labels = ['entailment' if label else 'neutral'],
                gold_label = 'entailment' if label else 'neutral',
                captionID = str(entry['claim_id']),
                evidenceID = str(entry['evidence_id']),
                pairID = str(entry['claim_id'])+'#'+str(i),
                sentence2 = entry['claim'],
                sentence1 = sent,
            )

        try:
            s1, s1b = get_parses(snli_entry['sentence1'])
            s2, s2b = get_parses(snli_entry['sentence2'])
            for thing in [s1, s1b, s2, s2b]:
                if type(thing) is list:
                    print('found list\r', file=sys.stderr)
                    [thing] = thing
        except ValueError:
            continue

        for thing in [s1, s1b, s2, s2b]:
            assert type(thing) is str

        snli_entry['sentence1_parse'] = s1
        snli_entry['sentence1_binary_parse'] = s1b
        snli_entry['sentence2_parse'] = s2
        snli_entry['sentence2_binary_parse'] = s2b
        
        snli_entries.append(snli_entry)
        numproc += 1

        #if numproc >= 32:
        #    break

    return snli_entries


dbpath = Path('./out2/db_2_pos-neg.json')
db = json.loads(dbpath.read_text())

pos, neg = db['positive'], db['negative']
pos, neg = pos[:100], neg[:100] # DEBUG

labels = {
            0: 'negative',
            1: 'positive',
            'negative': 0,
            'positive': 1
        }

props = dict(train=.33, valid=.33, test=.33)


train, valid, test = [], [], []
for label in (0, 1):
    entries = []
    print('processing entries for label %s'%labels[label])
    for entry in progressbar((neg, pos)[label], redirect_stdout=1):
        try:
            entries += entry_to_snliish_format(entry, label)
        except FileNotFoundError:
            print('couldn\'t find %s'%entry['evidence'], file=sys.stderr)
            continue
        except TypeError:
            # print('error parsing some sentence for %s'%entry['claim'], file=sys.stderr)
            continue
        except UnicodeDecodeError:
            continue
        # break

    print('INFO: total entries collected for label={}: {}'.format(label, len(entries)))
    
    random.shuffle(entries)
    n = len(entries)

    a, b = 0, int(n*props['train'])
    train += entries[slice(a, b)]
    a, b = b, b + int(n*props['valid'])
    valid += entries[slice(a, b)]
    test += entries[b:]


tasks = ['train', 'valid', 'test']
lists = [train, valid, test]
for i in range(3):
    random.shuffle(lists[i])
    with open('out2/db_2_pos-neg_snli-{}.jsonl'.format(tasks[i]), 'w') as f:
        for entry in lists[i]:
            f.write(json.dumps(entry) + '\n')
